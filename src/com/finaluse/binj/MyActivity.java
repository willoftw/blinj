package com.finaluse.binj;


import java.util.Timer;
import java.util.TimerTask;
import java.util.UUID;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import net.sqlcipher.Cursor;
import net.sqlcipher.database.SQLiteCursor;
import net.sqlcipher.database.SQLiteDatabase;





import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.view.Menu;
import android.view.View.OnClickListener;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.provider.Settings.Secure;
import android.text.Editable;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

@SuppressLint("NewApi")
public class MyActivity extends Activity implements BluetoothAdapter.LeScanCallback {

	final private static int STATE_BLUETOOTH_OFF = 1;
	final private static int STATE_DISCONNECTED = 2;
	final private static int STATE_CONNECTING = 3;
	final private static int STATE_CONNECTED = 4;
	private int state;
	private boolean scanStarted;
	private boolean scanning;

	private BluetoothAdapter bluetoothAdapter;
	private BluetoothDevice bluetoothDevice;

	private RFduinoService rfduinoService;

	private CustomCursorAdapter customAdapter;
	private PersonDatabaseHelper databaseHelper;
	private static final int ENTER_DATA_REQUEST_CODE = 1;
	private ListView listView;
	private TextView status_text_view;
	
	private ResettableTimer timer; 


	private static final String TAG = MyActivity.class.getSimpleName();

	private final BroadcastReceiver bluetoothStateReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			//            int state = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, 0);
			//            if (state == BluetoothAdapter.STATE_ON) {
			//                upgradeState(STATE_DISCONNECTED);
			//            } else if (state == BluetoothAdapter.STATE_OFF) {
			//                downgradeState(STATE_BLUETOOTH_OFF);
			//            }
		}
	};

	private final BroadcastReceiver scanModeReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			scanning = (bluetoothAdapter.getScanMode() != BluetoothAdapter.SCAN_MODE_NONE);
			scanStarted &= scanning;
			updateUi();
		}
	};

	private final ServiceConnection rfduinoServiceConnection = new ServiceConnection() {
		@Override
		public void onServiceConnected(ComponentName name, IBinder service) {
			Log.d(TAG,"On Service Connection");
			rfduinoService = ((RFduinoService.LocalBinder) service).getService();
			if (rfduinoService.initialize()) {
				if (rfduinoService.connect(bluetoothDevice.getAddress())) {
					Log.d("com.finaluse.binj","Connecting Sucess");
					upgradeState(STATE_CONNECTING);
				}
				else
				{
					Log.d(TAG,"Failed to connect to device :(");
				}
			}
		}

		@Override
		public void onServiceDisconnected(ComponentName name) {
			Log.d(TAG,"On Service Disconnection");
			rfduinoService = null;
			downgradeState(STATE_DISCONNECTED);
		}
	};

	private final BroadcastReceiver rfduinoReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			final String action = intent.getAction();
			if (RFduinoService.ACTION_CONNECTED.equals(action)) {
				upgradeState(STATE_CONNECTED);
			} else if (RFduinoService.ACTION_DISCONNECTED.equals(action)) {
				downgradeState(STATE_DISCONNECTED);
			} else if (RFduinoService.ACTION_DATA_AVAILABLE.equals(action)) {
				Log.d(TAG,"ACTION DATA REVCIEVED");

				// addData(intent.getByteArrayExtra(RFduinoService.EXTRA_DATA));
			}
		}
	};


	/**
	 * Called when the activity is first created.
	 */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_list);
		SQLiteDatabase.loadLibs(this);
		
	      int timeout = 5;
          int period = 5;
          final int nresetsPerPeriod = 5;
          ScheduledExecutorService scheduler = new ScheduledThreadPoolExecutor(1);
          timer = new ResettableTimer(scheduler, 
                  timeout, TimeUnit.SECONDS,
                  new Runnable() { 
                      public void run() { finish(); }
                  }
          );

		bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
		
		Button reset_button = (Button) findViewById(R.id.button_refresh);
		reset_button.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				recreate();
				
			}
		});

		AlertDialog.Builder alert = new AlertDialog.Builder(this);

		alert.setTitle("Enter Pin");
		alert.setMessage("PIN");

		// Set an EditText view to get user input 
		final EditText input = new EditText(this);
		input.setInputType(InputType.TYPE_CLASS_NUMBER| InputType.TYPE_NUMBER_VARIATION_PASSWORD);
		alert.setView(input);

		alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton) {
				String value = input.getText().toString();
				// Do something with value!
				databaseHelper = new PersonDatabaseHelper(getBaseContext());
				
				if (databaseHelper.CheckPass(value))
				{

					databaseHelper = new PersonDatabaseHelper(getBaseContext(),value);


					// Database query can be a time consuming task ..
					// so its safe to call database query in another thread
					// Handler, will handle this stuff for you <img src="http://s0.wp.com/wp-includes/images/smilies/icon_smile.gif?m=1129645325g" alt=":)" class="wp-smiley">

					new Handler().post(new Runnable() {
						@Override
						public void run() {
							customAdapter = new CustomCursorAdapter(MyActivity.this, databaseHelper.getAllData());
							listView.setAdapter(customAdapter);
						}
					});



					scanStarted = true;
					bluetoothAdapter.startLeScan(
							new UUID[]{ RFduinoService.UUID_SERVICE },
							MyActivity.this);
				}
				else
				{
					Toast.makeText(getBaseContext(), "Wrong Pin Fool", Toast.LENGTH_SHORT).show();
					recreate();
				}
			}
		}
				);

//		alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
//			public void onClick(DialogInterface dialog, int whichButton) {
//				// Canceled.
//			}
//		});

		alert.show();



		status_text_view = (TextView) findViewById(R.id.status_text_view);

		listView = (ListView) findViewById(R.id.list_data);
		listView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				Log.d(TAG, "clicked on item: " + parent.getAdapter().getItem(position).toString());
				Cursor cursor = (Cursor)parent.getAdapter().getItem(position);
				//                cursor.getString(0);
				Log.d(TAG, "got: " + cursor.getString(2));
				if(rfduinoService != null)
					rfduinoService.send(cursor.getString(2).getBytes());
				else
				{
					Toast.makeText(getBaseContext(), "Whoops, Not Connected!", Toast.LENGTH_SHORT).show();

				}
			}
		});

		listView.setOnItemLongClickListener(new OnItemLongClickListener() {

			public boolean onItemLongClick(AdapterView<?> parent, View arg1,
					int pos, long id) {
				// TODO Auto-generated method stub
				//Cursor cursor = parent.getAdapter().getItem(pos);
				databaseHelper.clearData();
				Log.v("long clicked","pos: " + pos);

				return true;
			}
		}); 




	}

	public void onClickEnterData(View btnAdd) {

		startActivityForResult(new Intent(this, EnterDataActivity.class), ENTER_DATA_REQUEST_CODE);

	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {

		super.onActivityResult(requestCode, resultCode, data);

		if (requestCode == ENTER_DATA_REQUEST_CODE && resultCode == RESULT_OK) {

			databaseHelper.insertData(data.getExtras().getString("entry_name"), data.getExtras().getString("pass"));

			customAdapter.changeCursor(databaseHelper.getAllData());
		}
	}

	@Override
	public void onLeScan(BluetoothDevice device, final int rssi, final byte[] scanRecord) {
		bluetoothAdapter.stopLeScan(this);
		bluetoothDevice = device;



		MyActivity.this.runOnUiThread(new Runnable() {
			@Override
			public void run() {
				status_text_view.setText("Found: "+bluetoothDevice.getName());
				Intent rfduinoIntent = new Intent(MyActivity.this, RFduinoService.class);
				bindService(rfduinoIntent, rfduinoServiceConnection, BIND_AUTO_CREATE);
				//deviceInfoText.setText(
				//        BluetoothHelper.getDeviceInfoText(bluetoothDevice, rssi, scanRecord));
				updateUi();
			}
		});
	}

	@Override
	protected void onStart() {
		super.onStart();

		registerReceiver(scanModeReceiver, new IntentFilter(BluetoothAdapter.ACTION_SCAN_MODE_CHANGED));
		registerReceiver(bluetoothStateReceiver, new IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED));
		registerReceiver(rfduinoReceiver, RFduinoService.getIntentFilter());

		updateState(bluetoothAdapter.isEnabled() ? STATE_DISCONNECTED : STATE_BLUETOOTH_OFF);
	}

	@Override
	protected void onStop() {
		super.onStop();

		bluetoothAdapter.stopLeScan(this);

		unregisterReceiver(scanModeReceiver);
		unregisterReceiver(bluetoothStateReceiver);
		unregisterReceiver(rfduinoReceiver);
	}


	private void upgradeState(int newState) {
		if (newState > state) {
			updateState(newState);
		}
	}

	private void downgradeState(int newState) {
		if (newState < state) {
			updateState(newState);
		}
	}

	private void updateState(int newState) {
		state = newState;
		updateUi();
	}

	private void updateUi() {

		// Scan
		if (scanStarted && scanning) {
			status_text_view.setText("Scanning...");
		} else if (scanStarted) {
			status_text_view.setText("Scan started...");
		} else {
			status_text_view.setText("");
		}

		// Connect
		boolean connected = false;
		String connectionText = "Disconnected";
		if (state == STATE_CONNECTING) {
			connectionText = "Connecting...";
		} else if (state == STATE_CONNECTED) {
			connected = true;
			connectionText = "Connected";
			status_text_view.setBackgroundColor(Color.GREEN);

			new Handler().postDelayed(new Runnable() {
				@Override
				public void run() {
					String android_id = Secure.getString(getBaseContext().getContentResolver(),
							Secure.ANDROID_ID); 
					android_id = android_id.substring(android_id.length()/2);
					Log.d("BTLE", ""+android_id);
					//

					rfduinoService.send(android_id.getBytes());
				}
			}, 1000);


		}
		else
		{
			connectionText = "Not Connected";
			status_text_view.setBackgroundColor(Color.RED);
		}
		status_text_view.setText(connectionText);
	}

	
	@Override
	public void onUserInteraction() {
		
		timer.reset(true);
	}
	



	
}
