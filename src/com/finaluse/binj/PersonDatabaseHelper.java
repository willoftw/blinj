package com.finaluse.binj;
import java.io.File;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import net.sqlcipher.SQLException;
import net.sqlcipher.database.SQLiteDatabase;
import net.sqlcipher.database.SQLiteException;
import net.sqlcipher.database.SQLiteOpenHelper;
import android.util.Log;
 
public class PersonDatabaseHelper {
 
    private static final String TAG = PersonDatabaseHelper.class.getSimpleName();
 
    // database configuration
    // if you want the onUpgrade to run then change the database_version
    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "mydatabase.db";
 
    // table configuration
    private static final String TABLE_NAME = "person_table";         // Table name
    private static final String PERSON_TABLE_COLUMN_ID = "_id";     // a column named "_id" is required for cursor
    private static final String PERSON_TABLE_COLUMN_NAME = "person_name";
    private static final String PERSON_TABLE_COLUMN_PIN = "person_pin";
 
    private DatabaseOpenHelper openHelper;
    private SQLiteDatabase database;
    private Context context;
    private String PASSWORD; 
    //private Boolean ;
 
    // this is a wrapper class. that means, from outside world, anyone will communicate with PersonDatabaseHelper,
    // but under the hood actually DatabaseOpenHelper class will perform database CRUD operations 
    public PersonDatabaseHelper(Context aContext,String pass) {
        openHelper = new DatabaseOpenHelper(aContext);
        this.context = aContext;
        PASSWORD = pass;
        InitializeSQLCipher();
        //database = openHelper.getWritableDatabase();
       
    }
    
    public PersonDatabaseHelper(Context aContext) {
        openHelper = new DatabaseOpenHelper(aContext);
        this.context = aContext;
        //InitializeSQLCipher();
        //database = openHelper.getWritableDatabase();
       
    }
    
    public Boolean CheckPass(String password)
    {
        SQLiteDatabase.loadLibs(context);
        File databaseFile = context.getDatabasePath("bluekey.db");
        databaseFile.mkdirs();
        //databaseFile.delete();
        try{
        SQLiteDatabase.openOrCreateDatabase(databaseFile, password, null);
        }
        catch(SQLiteException e)
        {
        	return false;
        }
        return true;
    }
    
    
    private void InitializeSQLCipher() {
        SQLiteDatabase.loadLibs(context);
        File databaseFile = context.getDatabasePath("bluekey.db");
        databaseFile.mkdirs();
        //databaseFile.delete();
        try{
        database = SQLiteDatabase.openOrCreateDatabase(databaseFile, PASSWORD, null);
        }
        catch(SQLiteException e)
        {
        	
        }
        //clearData();
        //database.execSQL("create table t1(a, b)");
        //database.execSQL("insert into t1(a, b) values(?, ?)", new Object[]{"one for the money",
        //                                                                "two for the show"});
    }
 
    public void insertData (String aPersonName, String aPersonPin) {
 
        // we are using ContentValues to avoid sql format errors
 
        ContentValues contentValues = new ContentValues();
 
        contentValues.put(PERSON_TABLE_COLUMN_NAME, aPersonName);
        contentValues.put(PERSON_TABLE_COLUMN_PIN, aPersonPin);
 
        database.insert(TABLE_NAME, null, contentValues);
    }
    
    public void clearData () {
    	 
        database.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        String buildSQL = "CREATE TABLE " + TABLE_NAME + "( " + PERSON_TABLE_COLUMN_ID + " INTEGER PRIMARY KEY, " +
                PERSON_TABLE_COLUMN_NAME + " TEXT, " + PERSON_TABLE_COLUMN_PIN + " TEXT )";

        Log.d(TAG, "onCreate SQL: " + buildSQL);

        database.execSQL(buildSQL);
 
    }
    
 
    public Cursor getAllData () {
 
        String buildSQL = "SELECT * FROM " + TABLE_NAME;
 
        Log.d(TAG, "getAllData SQL: " + buildSQL);
 
        return database.rawQuery(buildSQL, null);
    }
 
    // this DatabaseOpenHelper class will actually be used to perform database related operation 
     
    public String getPASSWORD() {
		return PASSWORD;
	}

	public void setPASSWORD(String pASSWORD) {
		PASSWORD = pASSWORD;
	}

	private class DatabaseOpenHelper extends SQLiteOpenHelper {
 
        public DatabaseOpenHelper(Context aContext) {
            super(aContext, DATABASE_NAME, null, DATABASE_VERSION);
        }
 
        @Override
        public void onCreate(SQLiteDatabase sqLiteDatabase) {
            // Create your tables here
 
            String buildSQL = "CREATE TABLE " + TABLE_NAME + "( " + PERSON_TABLE_COLUMN_ID + " INTEGER PRIMARY KEY, " +
                    PERSON_TABLE_COLUMN_NAME + " TEXT, " + PERSON_TABLE_COLUMN_PIN + " TEXT )";
 
            Log.d(TAG, "onCreate SQL: " + buildSQL);
 
            sqLiteDatabase.execSQL(buildSQL);
        }
 
        @Override
        public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) {
            // Database schema upgrade code goes here
 
            String buildSQL = "DROP TABLE IF EXISTS " + TABLE_NAME;
 
            Log.d(TAG, "onUpgrade SQL: " + buildSQL);
 
            sqLiteDatabase.execSQL(buildSQL);       // drop previous table
 
            onCreate(sqLiteDatabase);               // create the table from the beginning
        }
    }
}