package com.finaluse.binj;

import java.util.Random;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;
 
public class EnterDataActivity extends Activity {
 
    EditText editTextName;
    EditText editTextPass;
 
    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
 
        super.onCreate(savedInstanceState);
        setContentView(R.layout.enter_data);
 
        editTextName = (EditText) findViewById(R.id.add_entry_name);
        editTextPass = (EditText) findViewById(R.id.add_pass);
        editTextPass.setText(randomString(10));
    }
    
    static final String AB = "!?@�$%^&*()~`{}[]|0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
    static Random rnd = new Random();

    String randomString( int len ) 
    {
       StringBuilder sb = new StringBuilder( len );
       for( int i = 0; i < len; i++ ) 
          sb.append( AB.charAt( rnd.nextInt(AB.length()) ) );
       return sb.toString();
    }
 
    public void onClickAdd (View btnAdd) {
 
        String personName = editTextName.getText().toString();
        String personPIN = editTextPass.getText().toString();
 
        if ( personName.length() != 0 && personPIN.length() != 0 ) {
 
            Intent newIntent = getIntent();
            newIntent.putExtra("entry_name", personName);
            newIntent.putExtra("pass", personPIN);
 
            this.setResult(RESULT_OK, newIntent);
 
            finish();
        }
    }
    
    public void onClickAddAndEmail (View btnAdd) {
    	 
        String personName = editTextName.getText().toString();
        String personPIN = editTextPass.getText().toString();
 
        if ( personName.length() != 0 && personPIN.length() != 0 ) {
 
            Intent newIntent = getIntent();
            newIntent.putExtra("entry_name", personName);
            newIntent.putExtra("pass", personPIN);
 
            this.setResult(RESULT_OK, newIntent);
            
            Intent i = new Intent(Intent.ACTION_SEND);
            i.setType("message/rfc822");
            i.putExtra(Intent.EXTRA_EMAIL  , new String[]{"william.olner@gmail.com"});
            i.putExtra(Intent.EXTRA_SUBJECT, "BlueKey");
            i.putExtra(Intent.EXTRA_TEXT   , personName+" : " + personPIN);
            try {
                startActivity(Intent.createChooser(i, "Send mail..."));
            } catch (android.content.ActivityNotFoundException ex) {
                Toast.makeText(this, "There are no email clients installed.", Toast.LENGTH_SHORT).show();
            }
 
            finish();
        }
    }
    
}
