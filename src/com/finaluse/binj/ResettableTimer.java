package com.finaluse.binj;

import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;

public class ResettableTimer {
    final private ScheduledExecutorService scheduler;
    final private long timeout;
    final private TimeUnit timeUnit;
    final private Runnable task;
    final private AtomicReference<ScheduledFuture<?>> ticket
        = new AtomicReference<ScheduledFuture<?>>();
    /* use AtomicReference to manage concurrency 
     * in case reset() gets called from different threads
     */

    public ResettableTimer(ScheduledExecutorService scheduler, 
            long timeout, TimeUnit timeUnit, Runnable task)
    {
        this.scheduler = scheduler;
        this.timeout = timeout;
        this.timeUnit = timeUnit;
        this.task = task;
    }

    public ResettableTimer reset(boolean mayInterruptIfRunning) {
        /*
         *  in with the new, out with the old;
         *  this may mean that more than 1 task is scheduled at once for a short time,
         *  but that's not a big deal and avoids some complexity in this code 
         */
        ScheduledFuture<?> newTicket = this.scheduler.schedule(
                this.task, this.timeout, this.timeUnit);
        ScheduledFuture<?> oldTicket = this.ticket.getAndSet(newTicket);
        if (oldTicket != null)
        {
            oldTicket.cancel(mayInterruptIfRunning);
        }
        return this;
    }


    static public void main(String[] args)
    {
        if (args.length >= 3) 
        {
            int timeout = Integer.parseInt(args[0]);
            int period = Integer.parseInt(args[1]);
            final int nresetsPerPeriod = Integer.parseInt(args[2]);
            ScheduledExecutorService scheduler = new ScheduledThreadPoolExecutor(1);
            final ResettableTimer timer = new ResettableTimer(scheduler, 
                    timeout, TimeUnit.MILLISECONDS,
                    new Runnable() { 
                        public void run() { System.out.println("timeout!"); }
                    }
            );

            // start a separate thread pool for resetting
            new ScheduledThreadPoolExecutor(5).scheduleAtFixedRate(new Runnable() {
                private int runCounter = 0;
                public void run() { 
                    for (int i = 0; i < nresetsPerPeriod; ++i)
                    {
                        timer.reset(false);
                    }
                    if ((++this.runCounter % 100) == 0)
                    {
                        System.out.println("runCounter: "+this.runCounter);
                    }
                }
            }, 0, period, TimeUnit.MILLISECONDS);

            try 
            {
                while (true)
                {
                    Thread.sleep(1000);
                }
            }
            catch (InterruptedException e)
            {
                System.out.println("interrupted!");
            }
        }
    }
}
